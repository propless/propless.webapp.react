import axios, {AxiosInstance, AxiosResponse} from 'axios';
import MockAdapter from 'axios-mock-adapter';
import {StatusCodes} from 'http-status-codes';

const {NODE_ENV} = process.env;
export abstract class HTTPClient {
  private client: AxiosInstance;

  private mockAdapter: MockAdapter = null;

  private seeder: Faker.FakerStatic = null;

  constructor(props: HTTPClient.Props) {
    // The 'mocked' flag allows the user to safely leave mocks on
    // the production code without it causing unintended consequences.
    this.client = axios.create(props.config);

    if (NODE_ENV !== 'production' && props.allowMocking) {
      this.mockAdapter = new MockAdapter(this.client, {
        delayResponse: 1500,
        onNoMatch: 'throwException',
      });
      this.setSeeder();
    }
  }

  // Does not load on production mode.
  private async setSeeder() {
    this.seeder = await import('faker');
  }

  protected async get<TResponse>(
    params: HTTPClient.GetParams<TResponse>
  ): Promise<AxiosResponse<TResponse>> {
    if (this.mockAdapter && params.mock) this.mock().get(params);

    const response = await this.client.get<TResponse>(params.path, {
      ...params.options,
    });

    return response;
  }

  protected async post<TResponse>(
    params: HTTPClient.PostParams<TResponse>
  ): Promise<AxiosResponse<TResponse>> {
    if (this.mockAdapter && params.mock) this.mock().post(params);

    const response = await this.client.post<TResponse>(params.path, {
      ...params.options,
    });

    return response;
  }

  private mock() {
    const get = <TResponse>(params: HTTPClient.GetParams<TResponse>) => {
      this.mockAdapter
        .onGet(params.path, params.options)
        .replyOnce(
          params.mock.status || StatusCodes.OK,
          params.mock.builder(this.seeder)
        );
    };

    const post = <TResponse>(params: HTTPClient.PostParams<TResponse>) => {
      this.mockAdapter
        .onPost(params.path, params.options)
        .replyOnce(
          params.mock.status || StatusCodes.OK,
          params.mock.builder(this.seeder)
        );
    };

    return {get, post};
  }
}
