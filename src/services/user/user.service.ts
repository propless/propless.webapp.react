
import {HTTPClient} from '~/clients';

const {REACT_APP_IP_API} = process.env;

class Service extends HTTPClient {
  constructor() {
    super({
      config: {
        baseURL: REACT_APP_IP_API,
      },
      allowMocking: false,
    });
  }

  public async getClientIp(): Promise<UserService.GetClientIpDTO> {
    const response = await this.get<UserService.GetClientIpResponse>({
      path: '/json',
      options: {
        params: {
          fields: [
            'query',
            'country',
            'city',
            'region',
            'lat',
            'lon',
          ],
        },
      },
      mock: {
        builder: seeder => ({
          city: seeder.address.city(),
          country: seeder.address.country(),
          query: seeder.internet.ip(),
          region: seeder.address.state(),
          lat: parseInt(seeder.address.latitude()),
          lon: parseInt(seeder.address.longitude()),
        }),
      },
    });

    return {
      city: response.data.city,
      country: response.data.country,
      ip: response.data.query,
      state: response.data.region,
      geolocation: {
        latitude: response.data.lat,
        longitude: response.data.lon,
      },
    };
  }
}

export const UserService = new Service();
