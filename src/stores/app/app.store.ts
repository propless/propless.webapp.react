import {Action, Location} from 'history';
import {BaseStore} from '..';

class Store extends BaseStore<AppDataStore.State> {

  private static readonly HISTORY_LIMIT = 100;

  constructor() {
    super({
      browsing: {
        history: [ ],
      },
      clicks: 0,
      mousePosition: {
        x: 0,
        y: 0,
      },
    });

    window.addEventListener('mousemove', event => this.handleMouseMove(event));
    window.addEventListener('click', () => this.handleMouseClick());
  }

  private handleMouseMove({x, y}: MouseEvent): void {
    this.state.mousePosition.batch(prev => {
      prev.x.set(x);
      prev.y.set(y);
    });
  }

  private handleMouseClick(): void {
    this.state.clicks.set(prev => prev + 1);
  }

  public handleNavigationUpdate(event: {
    action: Action,
    location: Location,
  }): void {
    const {length} = this.state.browsing.history;
    if(length >= Store.HISTORY_LIMIT) {
      const start = (length + 1) - Store.HISTORY_LIMIT;
      this.state.browsing.history.set(prev => {
        return prev.slice(start, Store.HISTORY_LIMIT).concat(event);
      });
    } else {
      this.state.browsing.history.set(prev => prev.concat(event));
    }
  }

  // PS: Alternative batch update just in case the this.setState fails
  // to deliver the proper updates.
  // private handleMouseMove({x, y}: MouseEvent): void {
  //   this.state.batch(state => {
  //     state.mousePosition.x.set(() => x);
  //     state.mousePosition.y.set(() => y);
  //   }, this.getBatchUpdateKey(this.handleMouseMove));
  // }
}

export const AppStore = new Store();
