
import {UserService} from '~/services';

import {AppStore, BaseStore} from '../..';

class Store extends BaseStore<UserDataStore.State> {

  constructor() {
    super({
      ip: null,
      email: null,
      name: {
        first: null,
        last: null,
      },
      fullName: null,
      location: {
        city: null,
        country: null,
        latitude: null,
        longitude: null,
        state: null,
      },
      sessionStatus: false,
    }, {
      persistence: {
        storageKey: 'user-data-store',
      },
    });

    this.reaction({
      path: [ 'name' ],
      callback: (state) => state.sessionStatus.set(
        !!state.name.first.get() &&
        !!state.name.last.get()
      ),
    });

    this.reaction({
      path: [ 'name' ],
      callback: state => {
        const {first, last} = state.name;
        return state.fullName.set(`${first.get()} ${last.get()}`);
      },
    });
  }

  public async signUp(values: LoginPage.Form.Values) {
    await new Promise((resolve) => setTimeout(resolve, 2000));

    UserDataStore.state.batch(() => {
      UserDataStore.state.email.set(values.email);
      UserDataStore.state.name.set({
        first: values.firstName,
        last: values.lastName,
      });
    });
  }

  public signOut() {
    AppStore.resetState();
    this.resetState();
  }

  public async getIp() {
    const result = await UserService.getClientIp();
    this.state.batch(state => {
      state.ip.set(result.ip);
      state.location.set({
        city: result.city,
        country: result.country,
        latitude: result.geolocation.latitude,
        longitude: result.geolocation.longitude,
        state: result.state,
      });
    });
  }

}

export const UserDataStore = new Store();
