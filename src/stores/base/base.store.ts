import {
PluginCallbacksOnSetArgument, State, createState,
} from '@hookstate/core';
import {Persistence} from '@hookstate/persistence';
import {deepClone} from '~/utils';

export abstract class BaseStore<TState> {

  private _initialState: TState;
  private _state: State<TState>;
  private _reactions: Array<BaseStore.Reaction<TState>> = [ ];

  constructor(state: TState, config?: BaseStore.Config) {
    this._initialState = deepClone(state);
    this._state = createState(state);
    this._state.attach(() => ({
      id: Symbol('OnChangePlugin'),
      init: () => ({
        onSet: this.handleReactions.bind(this),
      }),
    }));

    if(config?.persistence) {
      this.state.attach(Persistence(config.persistence.storageKey));
    }
  }

  public get state(): State<TState> {
    return this._state;
  }

  public resetState(): void {
    this.state.set(deepClone(this._initialState));
  }

  public getBatchUpdateKey(caller: Function) {
    return `${this.constructor.name}_${caller.name}`;
  }

  protected reaction({callback, path}: BaseStore.Reaction<TState>): void {
    this._reactions.push({
      path,
      callback,
    });
  }

  private handleReactions(data: PluginCallbacksOnSetArgument) {
    this._reactions.forEach(r => {
      if(r.path.toString() === data.path.toString()) {
        r.callback(this.state, data);
      }
    });
  }
}
