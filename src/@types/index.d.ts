
import {ChangeEventHandler, FocusEventHandler} from 'react';
import {PluginCallbacksOnSetArgument, State} from '@hookstate/core';
import {AxiosRequestConfig} from 'axios';
import {Action, Location} from 'history';
import {StatusCodes} from 'http-status-codes';

type FakerStatic = Faker.FakerStatic;

declare global {
  namespace ExternalModules {
    namespace Axios {
      export type {AxiosRequestConfig};
    }
    namespace Faker {
      export type {FakerStatic};
    }
    namespace HTTPStatusCodes {
      export type {StatusCodes};
    }
    namespace History {
      export type  {
        Action,
        Location,
      };
    }
    namespace React {
      export type  {
        ChangeEventHandler,
        FocusEventHandler,
      };
    }
    namespace Hookstate {
      export type {State, PluginCallbacksOnSetArgument};
    }
  }
}
