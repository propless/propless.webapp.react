
declare namespace LoadingPartial {
  namespace Controller {
    type State = {
      requestCount: number;
    }
  }
}
