
import styled from 'styled-components';

const {PUBLIC_URL} = process.env;

export const Logo = styled.img.attrs({
  src: `${PUBLIC_URL}/images/logo512.png`,
})`
  display: block;
  height: 512px;
  width: 512px;
`;
