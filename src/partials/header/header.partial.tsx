
import {FC, useCallback} from 'react';
import {useHookstate} from '@hookstate/core';
import {useHistory} from 'react-router';
import {Routes} from '~/consts';
import {UserDataStore} from '~/stores';
import Brand from './brand/brand';
import {
 Link, LinkWrapper, Wrapper,
} from './header.partial.styles';

const Header: FC = () => {

  const {push} = useHistory();

  const navigate = useCallback((route: string) => {
    push(route);
  }, [ push ]);

  const {sessionStatus} = useHookstate(UserDataStore.state);

  return (
    <Wrapper>
      <Brand onClick={() => navigate(Routes.home)} />
      <LinkWrapper>
        {sessionStatus.value && (
          <Link onClick={() => navigate(Routes.home)}>
            Home
          </Link>
        )}
        <Link onClick={() => navigate(Routes.about)}>
          About
        </Link>
        {!sessionStatus.value && (
          <Link onClick={() => navigate(Routes.login)}>
            Login
          </Link>
        )}
        {sessionStatus.value && (
          <Link onClick={() => UserDataStore.signOut()}>
            Logout
          </Link>
        )}
      </LinkWrapper>
    </Wrapper>
  );
};

export default Header;
