
import styled, {css} from 'styled-components';

import {BaseInput, BaseLabel} from '~/atoms';

export const Wrapper = styled.div``;

export const Label = styled(BaseLabel)`
  font-size: 14px;
  margin-bottom: 3px;
`;

export const TextInput = styled(BaseInput)`
  margin-bottom: 3px;
`;

export const ErrorMessage = styled(BaseLabel)`
  font-size: 12px;
  
  ${({theme}) => css`
    color: ${theme.colors.failure};
  `}
`;
