
import {FC} from 'react';
import {ClientInfo} from '~/components';

import Form from './form/form';
import {Wrapper} from './login.page.styles';

const LoginPage: FC = () => (
  <Wrapper>
    <Form />
    <ClientInfo />
  </Wrapper>
);

export default LoginPage;
