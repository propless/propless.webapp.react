
import {FC} from 'react';
import {Formik} from 'formik';
import {LoadingPartialController} from '~/partials';
import {UserDataStore} from '~/stores';
import {ValidationUtil} from '~/utils';
import {
 EmailInput, Form as FForm, LastNameInput, NameInput, PasswordInput, SubmitButton, Title, Wrapper,
} from './form.styles';

type Values = LoginPage.Form.Values;

const Form: FC = () => {

  const handleFormSubmission = async(values: Values) => {
    LoadingPartialController.countRequest();
    try {
      await UserDataStore.signUp(values);
    } finally {
      LoadingPartialController.discountRequest();
    }
  };

  return (
    <Wrapper>
      <Formik<Values>
        initialValues={{
          email: '',
          firstName: '',
          lastName: '',
          password: '',
        }}
        onSubmit={handleFormSubmission}
        validationSchema={ValidationUtil.base().shape({
          email: ValidationUtil.email(),
          firstName: ValidationUtil.naming(),
          lastName: ValidationUtil.naming(),
          password: ValidationUtil.password(),
        })}
      >
        {({
          errors,
          handleBlur,
          handleChange,
          handleSubmit,
          touched,
          values,
        }) => (
          <FForm onSubmit={handleSubmit}>
            <Title>
              What's your name?
            </Title>
            <EmailInput
              onChange={handleChange}
              onBlur={handleBlur}
              value={values.email}
              errorMessage={touched.email ? errors.email : null}
            />
            <NameInput
              onChange={handleChange}
              onBlur={handleBlur}
              value={values.firstName}
              errorMessage={touched.firstName ? errors.firstName : null}
            />
            <LastNameInput
              onChange={handleChange}
              onBlur={handleBlur}
              value={values.lastName}
              errorMessage={touched.lastName ? errors.lastName : null}
            />
            <PasswordInput
              onChange={handleChange}
              onBlur={handleBlur}
              value={values.password}
              errorMessage={touched.password ? errors.password : null}
            />
            <SubmitButton>
              salvar
            </SubmitButton>
          </FForm>
        )}
      </Formik>
    </Wrapper>
  );
};

export default Form;
