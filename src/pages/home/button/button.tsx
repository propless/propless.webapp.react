
import {FC, useCallback} from 'react';
import {useHookstate} from '@hookstate/core';
import {AppStore, UserDataStore} from '~/stores';

import {HomePageController} from '../home.page.controller';
import {Wrapper} from './button.styles';

const Button: FC = () => {
  const {ip} = useHookstate(UserDataStore.state);
  const {isLoading} = useHookstate(HomePageController.state);

  const handleClick = useCallback(async() => {
    if(ip.get()) {
      AppStore.resetState();
      HomePageController.resetState();
      UserDataStore.resetState();
    } else {
      await HomePageController.getIp();
    }
  }, [ ip ]);

  return (
    <Wrapper
      onClick={handleClick}
      isLoading={isLoading.get()}
    >
      {(!ip.get() && !isLoading.get()) && 'find my ip'}
      {isLoading.get() && 'loading'}
      {(!!ip.get() && !isLoading.get()) && 'end session'}
    </Wrapper>
  );
};

export default Button;
