
export {default as HomePage} from './home/home.page';
export {default as AboutPage} from './about/about.page';
export {default as LoginPage} from './login/login.page';
